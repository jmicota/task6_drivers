#!/usr/pkg/bin/bash
set -e
set -x

mkdir /usr/src/minix/drivers/hello_queue

cp /hello_queue.c /usr/src/minix/drivers/hello_queue/hello_queue.c
cp /SO6/Makefile /usr/src/minix/drivers/hello_queue/Makefile
cp /SO6/hello_queue.h /usr/src/minix/drivers/hello_queue/hello_queue.h

cp /SO6/ioc_hello_queue.h /usr/include/sys/ioc_hello_queue.h
cp /SO6/ioc_hello_queue.h /usr/src/minix/include/sys/ioc_hello_queue.h

cp /SO6/hello_queue.h /SOTesty/hello_queue.h

cat /SO6/conf_update >> /etc/system.conf

cd /usr/src/minix/drivers/hello_queue
make clean
make
make install

mknod /dev/hello_queue c 17 0

service up /service/hello_queue -dev /dev/hello_queue
service update /service/hello_queue
service down hello_queue
