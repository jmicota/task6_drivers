#!/usr/bin/bash
cd ..
rm -rf RUNNING
mkdir RUNNING
mkdir RUNNING/COWBASE
cp BASE/minix.img RUNNING/COWBASE/backup_minix.img
cd RUNNING
qemu-img create -f qcow2 -o backing_file=COWBASE/backup_minix.img minix.img
