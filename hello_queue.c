#include <stdio.h>
#include <stdlib.h>
#include <minix/ds.h>
#include <minix/ioctl.h>
#include <minix/drivers.h>
#include <minix/chardriver.h>
#include <sys/ioc_hello_queue.h>
#include "hello_queue.h"

static char *hello_queue;
static size_t hq_device_size, hq_written_size;

static int hq_open(devminor_t minor, int access, endpoint_t user_endpt);
static int hq_close(devminor_t minor);
static ssize_t hq_read(devminor_t minor, u64_t position, endpoint_t endpt,
                       cp_grant_id_t grant, size_t size, int flags, cdev_id_t id);
static ssize_t hq_write(devminor_t UNUSED(minor), u64_t position, endpoint_t endpt,
                        cp_grant_id_t grant, size_t size, int UNUSED(flags), cdev_id_t UNUSED(id));
static int hq_ioctl(devminor_t UNUSED(minor), unsigned long request, endpoint_t endpt,
                    cp_grant_id_t grant, int UNUSED(flags), endpoint_t user_endpt, cdev_id_t UNUSED(id));

static void sef_local_startup(void);
static int sef_cb_init(int type, sef_init_info_t *info);
static int sef_cb_lu_state_save(int);
static int lu_state_restore(void);

static struct chardriver hello_queue_tab = {
    .cdr_open = hq_open,
    .cdr_close = hq_close,
    .cdr_read = hq_read,
    .cdr_write = hq_write,
    .cdr_ioctl = hq_ioctl
};

/* Fill buffer with initializing content. */
static void initialize()
{
    for (size_t i = 0; i < DEVICE_SIZE; i++)
    {
        if (i % 3 == 0)
            hello_queue[i] = 'x';
        if (i % 3 == 1)
            hello_queue[i] = 'y';
        if (i % 3 == 2)
            hello_queue[i] = 'z';
    }
    hq_device_size = DEVICE_SIZE;
    hq_written_size = DEVICE_SIZE;
}

static void memory_error() {
    printf("Out of memory.\n");
    exit(1);
}

static int hq_open(devminor_t UNUSED(minor), int UNUSED(access),
                   endpoint_t UNUSED(user_endpt)) {
    return OK;
}

static int hq_close(devminor_t UNUSED(minor)) {
    return OK;
}

static ssize_t hq_read(devminor_t UNUSED(minor), u64_t position, endpoint_t endpt,
                       cp_grant_id_t grant, size_t size, int UNUSED(flags), cdev_id_t UNUSED(id))
{
    int ret;
    if (hq_written_size == 0)
        return 0;

    /* Ignoring 'position' (its always 0). */
    if (size > hq_written_size)
        size = (size_t)hq_written_size;

    if ((ret = sys_safecopyto(endpt, grant, 0, (vir_bytes)hello_queue, size)) != OK)
        return ret;

    /* Move content that left to beginning of the queue. */
    if (hq_written_size > 0) {
        size_t pos = 0;
        for (size_t i = size; i < hq_written_size; i++) {
            hello_queue[pos] = hello_queue[i];
            pos++;
        }
    }
    hq_written_size -= size; /* Mark read */

    /* Decrease size of device if needed */
    if (hq_written_size <= hq_device_size / 4) 
    {
        hq_device_size /= 2;
        if (hq_device_size == 0)
            hq_device_size = 2;
        hello_queue = (char *)realloc(hello_queue, hq_device_size);
    }
    return size;
}

static ssize_t hq_write(devminor_t UNUSED(minor), u64_t position, endpoint_t endpt,
                        cp_grant_id_t grant, size_t size, int UNUSED(flags), cdev_id_t UNUSED(id))
{
    int ret;
    if (size == 0)
        return 0;

    /* Increase size of device if needed */
    while (size + hq_written_size > hq_device_size) 
        hq_device_size *= 2;
    hello_queue = (char *)realloc(hello_queue, hq_device_size);
    if (hello_queue == NULL)
        memory_error();

    if ((ret = sys_safecopyfrom(endpt, grant, 0, (vir_bytes)(hello_queue + hq_written_size), size)) != OK)
        return ret;

    hq_written_size += size; /* Mark written */
    return size;
}

static int hq_ioctl(devminor_t UNUSED(minor), unsigned long request, endpoint_t endpt,
                    cp_grant_id_t grant, int UNUSED(flags), endpoint_t user_endpt, cdev_id_t UNUSED(id))
{
    int rc;
    switch (request)
    {
        case HQIOCRES: 
        {
            hello_queue = (char *)realloc(hello_queue, DEVICE_SIZE);
            if (hello_queue == NULL)
                memory_error();
            initialize();
            break;
        }
        case HQIOCSET: 
        {
            /* Adjust size of queue and size of written content */
            if (hq_written_size < MSG_SIZE)
                hq_written_size = MSG_SIZE;
            
            if (hq_device_size < MSG_SIZE) {
                while (hq_device_size < MSG_SIZE)
                    hq_device_size *= 2;
                hello_queue = (char *)realloc(hello_queue, hq_device_size);
                if (hello_queue == NULL)
                    memory_error();
            }
            /* Write to last MSG_SIZE bytes of queue */
            rc = sys_safecopyfrom(endpt, grant, 0, (vir_bytes)(hello_queue + hq_written_size - MSG_SIZE), MSG_SIZE);
            break;
        }
        case HQIOCXCH: 
        {
            /* Load two characters */
            char c[2];
            rc = sys_safecopyfrom(endpt, grant, 0, (vir_bytes)c, 2);
            if (rc != OK)
                break;
            /* Make changes */
            for (size_t i = 0; i < hq_written_size; i++) {
                if (hello_queue[i] == c[0])
                    hello_queue[i] = c[1];
            }
            break;
        }
        case HQIOCDEL: 
        {
            char arr[hq_written_size];
            size_t pos = 0;
            /* Write 2 out of every 3 bytes to temporary array */
            for (size_t i = 0; i < hq_written_size; i++) {
                if (i % 3 != 2) {
                    arr[pos] = hello_queue[i];
                    pos++;
                }
            }
            /* Copy temporary array to queue */
            for (size_t i = 0; i < pos; i++) {
                hello_queue[i] = arr[i];
            }
            hq_written_size = pos;
            break;
        }
        default:
            rc = ENOTTY;
    }
    return rc;
}

static int sef_cb_lu_state_save(int UNUSED(state))
{
    ds_publish_mem("hello_queue", (void*)hello_queue, hq_written_size, DSF_OVERWRITE);
    ds_publish_u32("hq_device_size", hq_device_size, DSF_OVERWRITE);
    free(hello_queue);
    return OK;
}

static int lu_state_restore()
{
    ds_retrieve_u32("hq_device_size", &hq_device_size);
    hq_device_size = (size_t) hq_device_size;
    ds_delete_u32("hq_device_size");

    hello_queue = malloc(hq_device_size);
    if (hello_queue == NULL)
        memory_error();

    hq_written_size = hq_device_size; /* Will be decreased by ds_retrieve_mem */
    ds_retrieve_mem("hello_queue", hello_queue, &hq_written_size);
    ds_delete_mem("hello_queue");

    return OK;
}

static void sef_local_startup()
{
    sef_setcb_init_fresh(sef_cb_init);
    sef_setcb_init_lu(sef_cb_init);
    sef_setcb_init_restart(sef_cb_init);

    /* - Agree to update immediately when LU is requested in a valid state. */
    sef_setcb_lu_prepare(sef_cb_lu_prepare_always_ready);
    /* - Support live update starting from any standard state. */
    sef_setcb_lu_state_isvalid(sef_cb_lu_state_isvalid_standard);
    /* - Register a custom routine to save the state. */
    sef_setcb_lu_state_save(sef_cb_lu_state_save);

    sef_startup();
}

static int sef_cb_init(int type, sef_init_info_t *UNUSED(info))
{
    int do_announce_driver = TRUE;
    switch (type)
    {
        case SEF_INIT_FRESH:
            hello_queue = malloc(DEVICE_SIZE);
            if (hello_queue == NULL)
                memory_error();
            initialize();
            break;

        case SEF_INIT_LU:
            lu_state_restore();
            do_announce_driver = FALSE;
            break;

        case SEF_INIT_RESTART:
            break;
    }

    if (do_announce_driver)
        chardriver_announce();
    return OK;
}

int main(void)
{
    sef_local_startup();
    chardriver_task(&hello_queue_tab);
    free(hello_queue);
    return OK;
}