#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/ioc_hello_queue.h>

#include "hello_queue.h"

static void read_print_write(int fd, char* buf, size_t size) {
  ssize_t r = read(fd, buf, sizeof(char) * size);
  for (ssize_t i = 0; i < r; i++) {
    printf("%c", buf[i]);
  }
  printf("\n");
  write(fd, buf, sizeof(char) * r);
}

int main() {
  int fd;
  char change[2];
  char buf[DEVICE_SIZE];
  char msg[7] = "message";

  change[0] = 'x';
  change[1] = 'a';

  /* Open our queue. */
  if ((fd = open("/dev/hello_queue", O_RDWR)) < 0) {
    fprintf(stderr, "Error open\n");
    exit(1);
  }

  /* Call xch. */
  if (ioctl(fd, HQIOCXCH, change) < 0) {
    fprintf(stderr, "Error xch\n");
    exit(1);
  }
  printf("Letter x changed to a\n");
  read_print_write(fd, buf, DEVICE_SIZE);

  /* Call set. */
  if (ioctl(fd, HQIOCSET, msg) < 0) {
    fprintf(stderr, "Error set\n");
    exit(1);
  }
  printf("Set message\n");
  read_print_write(fd, buf, DEVICE_SIZE);

  /* Call del. */
  if (ioctl(fd, HQIOCDEL, msg) < 0) {
    fprintf(stderr, "Error del\n");
    exit(1);
  }
  printf("Delete each 3rd element\n");
  read_print_write(fd, buf, 10);

  /* Call res. */
  if (ioctl(fd, HQIOCRES) < 0) {
    fprintf(stderr, "Error res\n");
    exit(1);
  }
  printf("Reset queue to the default state\n");
  read_print_write(fd, buf, DEVICE_SIZE);

  close(fd);
}
